const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

const request = require('request');

var getSenderName = (message_id,call) => {
    request({
      url: 'https://graph.facebook.com/v2.10/m_' + message_id +'?fields=from',
      qs: {access_token: 'EAAZAWaKPRLdABAAWiewXgWyhZAfZAudpzx64DzJUF1TwmH2j3LzqZA0pinl6viZBl4qW4JYU8euIJeccthVNUPvRfiafuTeCsU4g2CtiyFUgbFFift0OHB1Y1BPKIRY1lHN6mPts6TEHXnCOHhKcqdfIsDjgLTy2iAsraiKHFjQZDZD'},
      method: 'GET',
      json: true
      }, (error, response, body) => {
        if (response.statusCode == 200) {
          console.log(JSON.stringify(body.from.name));
          return call(body);
        }  if (error) {
            console.log('Error sending message: ', error);
          } else if (response.body.error) {
            console.log('Error: ', response.body.error);
            }
      }
    )
}

module.exports = getSenderName;
