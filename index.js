const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//const _ = require('lodash');
//const async = require('async')

const request = require('request');
const apiai = require('apiai');
const apiaiApp = apiai('238e834121d64c9a949162a39b2491d1');
const token_fb = "EAAZAWaKPRLdABAAWiewXgWyhZAfZAudpzx64DzJUF1TwmH2j3LzqZA0pinl6viZBl4qW4JYU8euIJeccthVNUPvRfiafuTeCsU4g2CtiyFUgbFFift0OHB1Y1BPKIRY1lHN6mPts6TEHXnCOHhKcqdfIsDjgLTy2iAsraiKHFjQZDZD";

var getSenderName = require("./getSenderName.js");

var messageData = [];

//Configurar para que la aplicación escuche en el puerto 3000
const server = app.listen(process.env.PORT || 3000, () => {
  console.log('Express server listening on port %d in %s mode', server.address().port, app.settings.env);
});

/* Validación del bot de facebook con la aplicación */
app.get('/webhook', (req, res) => {
  if (req.query['hub.mode'] && req.query['hub.verify_token'] === '123456789') {
    res.status(200).send(req.query['hub.challenge']);
  } else {
    res.status(403).end();
  }
});

/* Envío de mensajes de Facebook al DialogFlow a través de un POST */
app.post('/webhook', (req, res) => {

  if (req.body.object === 'page') {
      //console.log('Entro al post / page:' + JSON.stringify(req.body));
      req.body.entry.forEach((entry) => {
          entry.messaging.forEach((event) => {
            if (event.message && event.message.text) {
                sendMessage_toAPI(event);
            }
        })
      })
    }
    res.status(200).end();
})


function sendMessage_toAPI(event) {
// enviar mensaje que le envió el Messenger al API
  let sender = event.sender.id;
  let text = event.message.text;
  let apiai = apiaiApp.textRequest(text, {
    sessionId: 'tabby_cat'
  });

  console.log('El text es:' + text);

/* Envia respuesta de API al texto anterior al Facebook */

  apiai.on('response', (response) => {

    i = 0;
    messageData = [];

    var APImessages = response.result.fulfillment.messages;

    console.log('Este es el APImessages: ' + JSON.stringify(APImessages));

/* Detectar cuando el API.ai está respondiendo con el Fallback Intent y cuando eso suceda enviar
mensaje de texto por Twilio. Además registrar al usuario en el archivo de puenteo */

    if(APImessages){
      if(APImessages.length > 0 ){

        getSenderName(event.message.mid,function(body){sendMessage_toFB(prepareTextMsg(sender,APImessages[0],body.from.name))});

      }
    }
  });

//Capturar los errores generados en el API
    apiai.on('error', (error) => {
      console.log(error);
    });

    apiai.end();

  };


  function prepareTextMsg(sender, APImessage,CompleteName) {
    var aiText = APImessage.speech;
    var Nombres = CompleteName.split(" ");
    var aiTextWithName = aiText.replace("amiguit@",Nombres[0]);
    console.log ('Texto de aiTextWithName: ' + aiTextWithName);
    if (aiText.length > 0){
      let textMsg = {
        recipient: {id: sender},
        message: {text: aiTextWithName}
      };
      return textMsg;
    }
    }

  function sendMessage_toFB(message) {
    console.log('El message data a enviar a FB es:' + JSON.stringify(message));
    request({
      url: 'https://graph.facebook.com/v2.10/me/messages',
      qs: {access_token: token_fb},
      method: 'POST',
      json: message
    }, (error, response) => {
      if (error) {
        console.log('Error sending message: ', error);
      } else if (response.body.error) {
        console.log('Error: ', response.body.error);
        }
      });
    }
